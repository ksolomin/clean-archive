import os
import shutil
import subprocess
import sys


def test_clean_up():
    for i in range(1, 8):
        print(f"Starting test{i}...")
        shutil.make_archive("test", "zip", root_dir=f"tests/data/test{i}")
        try:
            subprocess.check_call([sys.executable, "clean_app.py", "test.zip"])
            shutil.unpack_archive("test_new.zip", "test_new")
            subprocess.check_call(["diff", f"tests/data/test{i}_out", "test_new"])
        finally:
            os.remove("test.zip")
            os.remove("test_new.zip")
            shutil.rmtree("test_new")


def test_logging():
    with open("clean_app.py") as f:
        assert "logging" in f.read(), "Please add logging"
